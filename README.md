# Running this on a linux machine

```bash
# create the volume where your world will be stored
docker volume create --name tekkit
```

## Run the container

```bash
# make sure it is not running
docker kill tekkit

# remove the old container instance
docker container rm tekkit

# start the tekkit server in the background
docker run --name tekkit -d -p 25565:25565 -v tekkit:/opt/tekkit registry.gitlab.com/aapjeisbaas/tekkit

# tail the server logs
docker logs tekkit -f
```

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Ftekkit%2FREADME.md&dt=tekkit)

