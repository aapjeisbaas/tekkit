FROM java:8-jdk
WORKDIR /opt/tekkit/
RUN curl -o /tmp/tekkit.zip http://servers.technicpack.net/Technic/servers/tekkitmain/Tekkit_Server_v1.2.9g.zip \  
    && unzip /tmp/tekkit.zip -d /opt/tekkit/ \
    && chmod +x /opt/tekkit/launch.sh

EXPOSE 25565
VOLUME [ "/opt/tekkit" ]
CMD ["bash", "-c", "/opt/tekkit/launch.sh"]